import 'babel-polyfill';
import 'whatwg-fetch';
import { Menu } from './menu';

export const Portfolio = (() => {
    const allPortfolio = new Array();
    const webPortfolio = new Array();
    const appPortfolio = new Array();
    const iconPortfolio = new Array();
    const portfolioContent = document.querySelector('.tabs .tabs__content');
    const loadMoreBtn = document.querySelector('.tabs__load__btn');
    const itensLimit = 4;
    let totalItens;
    let totalPages;
    let currentPage = 1;

    const getData = () => {
        const url = './assets/json/portfolio.json';

        fetch(url).then(result => result.json())
            .then(json => {
                fillPortfolioArrays(json.portfolio);
            });
    }

    const fillPortfolioArrays = data => {
        data.filter(e => {
            if (e.type === "web") {
                webPortfolio.push(e);
            } else if (e.type === "app") {
                appPortfolio.push(e);
            } else if (e.type === "icon") {
                iconPortfolio.push(e);
            }

            allPortfolio.push(e);
        });

        totalItens = allPortfolio.length;
        totalPages = Math.round(totalItens / itensLimit);

        loadContent(allPortfolio, itensLimit);
        setTimeout(Menu.updateSectionOffset, 50);
    };

    const portfolioComponent = data => {
        const tabItem = document.createElement('div');
        const tabItemImage = new Image();
        const tabItemText = document.createElement('p');

        tabItem.className = 'tabs__content__item';
        tabItemImage.src = data.image;
        tabItemImage.setAttribute('alt', 'CUDA - ' + data.title);
        tabItemText.textContent = data.title;

        tabItem.appendChild(tabItemImage);
        tabItem.appendChild(tabItemText);

        return tabItem;
    }

    const loadContent = (array, limit) => {
        let cont = 0;

        if (array.length <= itensLimit || currentPage === totalPages) {
            loadMoreBtn.style.display = 'none';
        } else {
            loadMoreBtn.style.display = 'block';
        }

        while (portfolioContent.firstChild) {
            portfolioContent.removeChild(portfolioContent.firstChild);
        }

        array.map(e => {
            if (cont < limit) {
                const item = portfolioComponent(e);

                portfolioContent.appendChild(item);

                cont++;
            }
        });
    }

    const selectTab = () => {
        const tab = Array.prototype.slice.call(document.querySelectorAll('.tabs__header__item'));

        tab.map(e => {
            e.addEventListener('click', function () {
                const type = this.dataset.type;
                const tabActive = document.querySelector('.tabs__header__item--active');

                tabActive.classList.remove('tabs__header__item--active');
                this.classList.add('tabs__header__item--active');

                currentPage = 1;

                switch (type) {
                    case 'all':
                        loadContent(allPortfolio, itensLimit);
                        Menu.updateSectionOffset();
                        break;
                    case 'web':
                        loadContent(webPortfolio, itensLimit);
                        Menu.updateSectionOffset();
                        break;
                    case 'app':
                        loadContent(appPortfolio, itensLimit);
                        Menu.updateSectionOffset();
                        break;
                    case 'icon':
                        loadContent(iconPortfolio, itensLimit);
                        Menu.updateSectionOffset();
                        break;
                    default:
                        console.error(type + ' desconhecido');
                }
            }, false);
        });
    }

    const loadMore = () => {
        loadMoreBtn.addEventListener('click', function () {
            const tabActive = document.querySelector('.tabs__header__item--active');
            const type = tabActive.dataset.type;
            let numItens;

            currentPage++;

            switch (type) {
                case 'all':
                    if (currentPage * itensLimit > allPortfolio.length) {
                        numItens = allPortfolio.length;
                    } else {
                        numItens = currentPage * itensLimit;
                    }

                    loadContent(allPortfolio, numItens);
                    Menu.updateSectionOffset();
                    break;
                case 'web':
                    if (currentPage * itensLimit > webPortfolio.length) {
                        numItens = webPortfolio.length;
                    } else {
                        numItens = currentPage * itensLimit;
                    }

                    loadContent(webPortfolio, numItens);
                    Menu.updateSectionOffset();
                    break;
                case 'app':
                    if (currentPage * itensLimit > appPortfolio.length) {
                        numItens = appPortfolio.length;
                    } else {
                        numItens = currentPage * itensLimit;
                    }

                    loadContent(appPortfolio, numItens);
                    Menu.updateSectionOffset();
                    break;
                case 'icon':
                    if (currentPage * itensLimit > iconPortfolio.length) {
                        numItens = iconPortfolio.length;
                    } else {
                        numItens = currentPage * itensLimit;
                    }

                    loadContent(iconPortfolio, numItens);
                    Menu.updateSectionOffset();
                    break;
                default:
                    console.error(type + ' desconhecido');
            }
        }, false);
    }

    return {
        init: () => {
            getData();
            selectTab();
            loadMore();
        }
    }
})();