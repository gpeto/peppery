export const Form = (() => {
    const fakePlaceholder = Array.prototype.slice.call(document.querySelectorAll('.contact__form__row__placeholder'));
    const formInputs = Array.prototype.slice.call(document.querySelectorAll('.contact input, textarea'));

    /**
     * @func formPlaceholder
     * @desc Simula o comportamento de um placeholder: some ao clicar no campo e volta ao sair, se ele estiver vazio.
     */
    const formPlaceholder = () => {

        fakePlaceholder.map(e => {
            e.addEventListener('click', function () {
                this.style.display = 'none';
                this.previousElementSibling.focus();
            }, false);
        });

        formInputs.map(e => {
            e.addEventListener('blur', function () {
                if (this.value === "") {
                    this.nextElementSibling.style.display = 'block';
                }
            });

            e.addEventListener('focus', function () {
                this.nextElementSibling.style.display = 'none';
            });
        });
    };

    const formIsValid = () => {
        let message = null;
        const regx = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        formInputs.map(e => {
            if (e.value === "") {
                if (message === null) {
                    message = e.name + ' cannot be empty!';
                } else {
                    message += '\n' + e.name + ' cannot be empty!';
                }
            } else if (e.name === 'email') {
                if (!regx.test(e.value) && message != null) {
                    message += '\n' + e.name + ' is not valid!';
                } else if (!regx.test(e.value) && message === null) {
                    message = e.name + ' is not valid!';
                }
            }
        });

        if (message != null) {
            alert(message);
            return false;
        } else {
            return true;
        }
    };

    const formSubmit = () => {
        const formSubmitBtn = document.querySelector('.contact__form__row__submit');
        const form = document.querySelector('.contact__form');

        formSubmitBtn.addEventListener('click', function (e) {
            e.preventDefault();

            if (formIsValid()) {
                alert("Message sent! Thank you!");
                location.reload();
            }

            return false;
        }, false);
    };

    return {
        init: () => {
            formPlaceholder();
            formSubmit();
        }
    }
})();
