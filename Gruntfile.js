module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        connect: {
            preview: {
                options: {
                    port: 9000,
                    hostname: '*',
                    base: 'preview/',
                    open: true
                }
            }
        },

        watch: {
            options: {
                livereload: true,
                spawn: false,
                interrupt: true,
                event: ['added', 'changed', 'deleted']
            },

            html: {
                files: ['src/views/**/*.html'],
                tasks: ['includereplace:preview']
            },

            scss: {
                files: ['src/assets/scss/**/*.scss'],
                tasks: ['compass:preview']
            },

            js: {
                files: ['src/assets/js/**/*.js'],
                tasks: ['newer:browserify:preview']
            },

            img: {
                files: ['src/assets/img/**'],
                tasks: ['newer:copy:preview']
            }
        },

        compass: {
            preview: {
                options: {
                    sassDir: "./src/assets/scss/",
                    cssDir: "./preview/assets/css/"
                }
            },
            dist: {
                options: {
                    outputStyle: "compressed",
                    sassDir: './src/assets/scss/',
                    cssDir: './dist/assets/css/'
                }
            }
        },

        includereplace: {
            preview: {
                options: {
                    globals: {
                        baseCssURL: './assets/css',
                        baseJsURL: './assets/js',
                        baseImgURL: './assets/img',
                        livereloadScript: '<!--[if !IE]><!--><script src="//localhost:35729/livereload.js"></script><!--<![endif]-->'
                    }
                },
                files: [{
                    cwd: './src/views/',
                    src: [
                        '{,*/}*.html',
                        '!includes/**/*.html',
                        '!templates/**/*.html'
                    ],
                    dest: './preview/',
                    expand: true,
                }]
            },
            dist: {
                options: {
                    globals: {
                        baseCssURL: './assets/css',
                        baseImgURL: './assets/img',
                        baseJsURL: './assets/js',
                        livereloadScript: ''
                    }
                },
                files: [{
                    cwd: './src/views/',
                    src: [
                        '{,*/}*.html',
                        '!includes/**/*.html',
                        '!templates/**/*.html'
                    ],
                    dest: './dist/',
                    expand: true,
                }]
            }
        },
        copy: {
            preview: {
                files: [{
                    expand: true,
                    cwd: './src',
                    src: [
                        'assets/fonts/**',
                        'assets/img/**',
                        'assets/json/**'
                    ],
                    dest: './preview/'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: './src',
                    src: [
                        'assets/fonts/**',
                        'assets/img/**',
                        'assets/json/**'
                    ],
                    dest: './dist/'
                }]
            }
        },
        clean: {
            preview: ['./preview'],
            dist: ['./dist']
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: './dist/',
                    src: [
                        '**/*.html',
                        '*.html'
                    ],
                    dest: './dist/'
                }]
            }
        },
        kraken: {
            dist: {
                options: {
                    key: '13a12152e747158467b735a4bb122494',
                    secret: '84df7c19e8624c40bba472936e032b557ff36809',
                    lossy: true
                },
                files: [{
                    expand: true,
                    cwd: './src/assets/img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: './dist/assets/img/'
                }]
            }
        },

        browserify: {
            preview: {
                src: "./src/assets/js/dev/*.js",
                dest: './preview/assets/js/dev/bundle.js',
                options: {
                    browserifyOptions: { debug: true },
                    transform: [["babelify", { "presets": ["env"] }]]
                }
            },

            dist: {
                src: "./src/assets/js/dev/*.js",
                dest: './dist/assets/js/dev/bundle.js',
                options: {
                    browserifyOptions: { debug: false },
                    transform: [["babelify", { "presets": ["env"] }]],
                    plugin: [
                        ["minifyify", { map: false }]
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-include-replace');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-contrib-kraken');
	grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.registerTask('default', ['server']);

    grunt.registerTask('server', [
        'build',
        'connect:preview',
        'watch'
    ]);

    grunt.registerTask('build', [
        'clean:preview',
        'includereplace:preview',
        'compass:preview',
        'browserify:preview',
        'copy:preview',
    ]);

    grunt.registerTask('dist', [
        'clean:dist',
        'includereplace:dist',
        'htmlmin:dist',
        'compass:dist',
        'browserify:dist',
        'copy:dist',
        'kraken:dist'
    ]);
}