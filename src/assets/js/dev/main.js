import { Form } from './form';
import { Menu } from './menu';
import { Portfolio } from './portfolio';

const Main = (() => {
    return {
        init: () => {
            Form.init();
            Menu.init();
            Portfolio.init();
        }
    }
})();

Main.init();