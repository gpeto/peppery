export const Menu = (() => {
    let viewWidth = window.innerWidth;
    const menu = document.querySelector('.menu');
    const sections = {
        home: 0,
        services: document.querySelector('.services').offsetTop,
        team: document.querySelector('.team').offsetTop,
        skills: document.querySelector('.skills').offsetTop,
        portfolio: document.querySelector('.portfolio').offsetTop,
        testimonials: document.querySelector('.testimonials').offsetTop,
        contact: document.querySelector('.contact').offsetTop
    };

    const updateViewWidth = () => {
        window.onresize = function () {
            viewWidth = window.innerWidth;

            // Atualizando a distâncias de cada sessão em relação ao topo de acordo com a nova largura da página
            Menu.updateSectionOffset();
        }
    };

    /**
     * @func clickMenuMobile
     * @desc Clique no ícone 'sandwich' para abrir o menu no dispositivo ou em telas menores
     */
    const clickMenuMobile = () => {
        const mobileBtn = document.querySelector('.menu__content__navigation__mobile');
        const menuMobile = document.querySelector('.menu__mobile')
    
        mobileBtn.addEventListener('click', function () {
            menuMobile.classList.toggle('menu__mobile--open');
        }, false);
    }

    /**
    * @func scrollTo
    * @desc Animação de scroll da página usando JS puro - retirado de: https://gist.github.com/andjosh/6764939 @kimwes
    * @param {number} to 
    * @param {number} duration 
    */
    const scrollTo = (to, duration) => {
        const element = document.scrollingElement || document.documentElement,
            start = element.scrollTop,
            change = to - start,
            startDate = +new Date(),

            // t = current time
            // b = start value
            // c = change in value
            // d = duration
            easeInOutQuad = function (t, b, c, d) {
                t /= d / 2;
                if (t < 1) return c / 2 * t * t + b;
                t--;
                return -c / 2 * (t * (t - 2) - 1) + b;
            },

            animateScroll = function () {
                const currentDate = +new Date();
                const currentTime = currentDate - startDate;
                element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration));
                if (currentTime < duration) {
                    requestAnimationFrame(animateScroll);
                }
                else {
                    element.scrollTop = to;
                }
            };
        animateScroll();
    };

    /**
     * @func updateMenuActive
     * @desc Ao rolar a página, verifica se houve troca do bloco do site e atualiza o menu de acordo com o bloco ativo (visível na tela)
     * @param {number} position 
     */
    const updateMenuActive = position => {
        Object.keys( sections ).map( e => {
            const menuActive = document.querySelector( '.menu__content__navigation__item--active' );

            if(position >= sections[ e ]) {
                menuActive.classList.remove( 'menu__content__navigation__item--active' );
                document.querySelector( '.menu__content__navigation__item[data-target="' + e + '"]' ).classList.add( 'menu__content__navigation__item--active' );
            }
        });
    }

    const checkScroll = () => {
        window.onscroll = function () {
            updateMenuActive( window.pageYOffset );

            // Se a página for rolada e não estiver mais no topo, muda a aparência do menu
            // A partir dos 850px pra baixo, o menu mantém a aparência desde o topo
            if (viewWidth > 850) {
                if (pageYOffset >= 100) {
                    menu.classList.add('menu--scrolled');
                }
    
                if (this.pageYOffset === 0) {
                    menu.classList.remove('menu--scrolled');
                }
            } else {
                menu.classList.remove('menu--scrolled');
            }
        }
    };

    const navigation = () => {
        const menuItem = Array.prototype.slice.call( document.querySelectorAll( ' .menu__content__navigation__item' ));
        const menuMobileItem = Array.prototype.slice.call( document.querySelectorAll( ' .menu__mobile__item' ));
        const menuMobile = document.querySelector( '.menu__mobile' );
    
        menuItem.map(e => {
            e.addEventListener('click', function () {
                const target = this.dataset.target;
    
                scrollTo(sections[target], 1000);
            }, false);
        });

        menuMobileItem.map(e => {
            e.addEventListener('click', function () {
                const target = this.dataset.target;
                menuMobile.classList.remove( 'menu__mobile--open' );
                scrollTo(sections[target], 1000);
            }, false);
        });
    };

    return {
        init: () => {
            navigation();
            clickMenuMobile();
            checkScroll();
            updateViewWidth();
        },

        updateSectionOffset: () => {
            Object.keys(sections).map(e => {
                sections[e] = document.querySelector( '.' + e ).offsetTop;
            });
        }
    }
})();


