#COMO INICIAR
1. rode `git clone git@bitbucket.org:gpeto/peppery.git`
2. dentro da pasta do projeto: `npm i` para instalar todas as dependências
3. `grunt` para iniciar o servidor local de desenvolvimento
4. `grunt dist` para gerar os arquivos de produção
5. pronto!

***

#ESTRUTURA DO PROJETO

##PASTA DE DESENVOLVIMENTO
`src` é a pasta que você vai querer alterar quando estiver desenvolvendo o projeto. Ali você vai encontrar:

```
    src/
    |   assets/
    |   |   fonts/
    |   |   img/
    |   |   js/
    |   |   |   dev/ --> seus arquivos JS
    |   |   |   third-party/ --> arquivos JS de terceiros (plugins, libs e etc)
    |   |   scss/
    |   views/
    |   |   includes/ --> HTML que estará presente em todas as páginas, como chamadas de scripts e arquivos css
    |   |   templates/ --> HTML de cada parte do seu projeto, separado como módulos pra facilitar a manutenção
    |   |   index.html    
```

***

#O QUE ESSE PROJETO USA

1. ES6 Javascript
2. Compass
3. Otimização de imagem com Kraken
4. Minificação HTML com HTMLmin
5. Feito com base nos testes da Google Speedtest!
6. Grunt
